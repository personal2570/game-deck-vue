import { createApp } from 'vue';
import { createPinia } from 'pinia';

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

import Vue3Toasity, { toast } from 'vue3-toastify';
import 'vue3-toastify/dist/index.css';

import './style.css';
import App from './App.vue';


const pinia = createPinia();
const app = createApp(App);

app.use(pinia);

app.use(Vue3Toasity, {
    autoClose: 5000,
    position: toast.POSITION.TOP_CENTER
});

app.mount('#app');
