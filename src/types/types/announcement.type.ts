export const ANNOUNCEMENT_TYPE_SUCCESS = 'success';
export const ANNOUNCEMENT_TYPE_INFO = 'info';
export const ANNOUNCEMENT_TYPE_ERROR = 'error';

export type AnnouncementType = typeof ANNOUNCEMENT_TYPE_SUCCESS | typeof ANNOUNCEMENT_TYPE_INFO | typeof ANNOUNCEMENT_TYPE_ERROR;