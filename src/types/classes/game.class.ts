import { Announcement } from "./announcement.class";
import { ANNOUNCEMENT_TYPE_SUCCESS, ANNOUNCEMENT_TYPE_INFO, ANNOUNCEMENT_TYPE_ERROR } from '../types/announcement.type'
import { Card } from "./card.class";
import { HandCards } from "./handCards.class";
import { Deck } from "./deck.class";
import { GameMatch } from "../interfaces/gameMatch.interface";
import { Player } from "./player.class";

export class Game {

    private __players: Map<string, Player>;

    private __cardDeck: Deck;

    private __match = <GameMatch>{
        largestCard: null,
        largestCardHolder: null,
    }

    private announcement: Announcement;

    constructor() {

        const players = new Map();

        const competitor = ["A", "B"];

        for(let i = 0; i < competitor.length; i++) {
            const player = new Player(competitor[i]);
            players.set(player.getId, player);
        }

        this.__cardDeck = new Deck();

        this.__players = players;

        this.announcement = new Announcement();

    }

    get getPlayers() : Map<string, Player> {
        return this.__players;
    }

    get getDeck() : Deck {
        return this.__cardDeck;
    }

    get getAnnouncement() : Announcement {
        return this.announcement;
    }

    drawCards() : void {

        if ( this.__cardDeck.size >= 2 ) {

            for (const [key, player] of this.__players) {

                const card = this.__cardDeck.drawCard();

                this.getLargestCardHolder(player, card);

            }

            this.calculateScore();

        } else {

            this.announcement = new Announcement(
                ANNOUNCEMENT_TYPE_ERROR,
                `No more cards in the deck`
            );

        }

    }

    private getLargestCardHolder(player: Player, card: Card) : void {


        if ( this.__match.largestCard === null && this.__match.largestCardHolder === null) {
            this.__match.largestCardHolder = player;
            this.__match.largestCard = card;
        }


        if (  this.__match.largestCard !== null && this.__match.largestCard.rank > card.rank ) {
            this.__match.largestCardHolder = player;
            this.__match.largestCard = card;
        }

        player.addHandCard(new HandCards(card));

    }

    private calculateScore() : void {

        if ( this.__match.largestCardHolder && this.__match.largestCardHolder) {

            this.__match.largestCardHolder.addScore();

            this.__match.largestCardHolder.updateHandCardResult();

        }

        this.showAnnouncement();

        this.resetMatchValue();

    }

    private showAnnouncement() : void {

        let previousPlayerInfo: Player|null = null;

        for (const [key, player] of this.__players) {

            const playerScore = player.getScore;

            if ( previousPlayerInfo === null) previousPlayerInfo = player;

            if ( previousPlayerInfo.getScore < playerScore ) {

                this.announcement = new Announcement(
                    ANNOUNCEMENT_TYPE_SUCCESS,
                    `Player ${player.getId} has the most winning point ( ${player.getScore} ).`
                );

            }

            if ( previousPlayerInfo.getScore > playerScore ) {

                this.announcement = new Announcement(
                    ANNOUNCEMENT_TYPE_SUCCESS,
                    `Player ${previousPlayerInfo.getId} has the most winning point ( ${previousPlayerInfo.getScore} ).`
                );

            }

            if ( previousPlayerInfo.getScore === playerScore) {

                this.announcement = new Announcement(
                    ANNOUNCEMENT_TYPE_INFO,
                    `Both players have the same winning point ( ${player.getScore} ).`
                );
            }

        }

    }

    private resetMatchValue() : void {

        this.__match = <GameMatch>{
            largestCard: null as Card|null,
            largestCardHolder: null as Player|null,
        };

    }

}