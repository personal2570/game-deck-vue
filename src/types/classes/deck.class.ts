import { Card } from "./card.class";

export class Deck {

    private __cards: Card[];

    constructor() {

        const suits = ["spades", "diamonds", "clubs"];
        const values = ["king", "queen", "jack", "10", "9", "8", "7", "6", "5", "4", "3", "2", "ace"];

        const deck = new Array();

        for(let i = 0; i < suits.length; i++) {
            for(let x = 0; x < values.length; x++) {
                const previousRanking = (deck.length > 0 ) ? deck.length + 10 : 0;
                const card = new Card(suits[i], values[x], previousRanking + 1 )
                deck.push(card);
            }
        }

        this.__cards = deck;

    }

    get size() : number {
        return this.__cards.length;
    }

    drawCard() : Card {
        const randomNumber = Math.floor(Math.random() * (this.__cards.length));
        const currentCard = this.__cards.splice(randomNumber, 1);
        return currentCard[0];
    }

}