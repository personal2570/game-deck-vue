import { HandCards } from "./card.class";

export class Player {

    constructor(
        private id: string,
        private score: number = 0,
        private handCards: HandCards[] = []
    ) {}

    get getId() : string {
        return this.id;
    }

    get getScore(): number {
        return this.score;
    }

    get getCards() : HandCards[] {
        return this.handCards;
    }

    addHandCard(card : HandCards) : void {
        this.handCards.unshift(card);
    }

    addScore() : void {
        this.score++;
    }

    updateHandCardResult() : void {
        this.handCards[0].updateResult();
    }

}