import { Card } from "./card.class";

export class HandCards extends Card {

    constructor(
        card: Card,
        private type: boolean = false
    ) {
        const cardInfo = card.getImage.split("_");
        super(cardInfo[0], cardInfo[1], card.rank);
    }

    get getResult() : boolean {
        return this.type;
    }

    updateResult() : boolean {
        return this.type = true;
    }

}