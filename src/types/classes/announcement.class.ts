import { AnnouncementType } from "../types/announcement.type";
import { Announcement as AnnouncementObject } from "../interfaces/announcement.interface";

export class Announcement {

    constructor(
        protected type: AnnouncementType = 'info',
        protected message: string = '',
    ) {}

    get show() : AnnouncementObject {
        return {
            type: this.type,
            message: this.message
        };
    }

}