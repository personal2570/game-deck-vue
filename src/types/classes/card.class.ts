export class Card {

    constructor(
        protected suits: string,
        protected value: string,
        public  rank: number
    ) {}

    get getRanking(): string {
        return `${this.rank}`;
    }

    get getImage(): string {
        return `${this.suits}_${this.value}`;
    }

}