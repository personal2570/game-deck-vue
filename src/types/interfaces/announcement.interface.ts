import { AnnouncementType } from "../types/announcement.type";

export interface Announcement {
    type: AnnouncementType
    message: string;
}