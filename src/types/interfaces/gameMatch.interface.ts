import { Card } from "../classes/card.class";
import { Player } from "../classes/player.class";

export interface GameMatch {
    largestCard: Card|null;
    largestCardHolder: Player|null
}