import { defineStore } from 'pinia';
import { Game } from '../types/classes/game.class';
import { Player } from '../types/classes/player.class';

interface GameStore {
    game : Game;
}

export const useGameStore = defineStore('game', {

    state: () : GameStore => {
        return {
            // all these properties will have their type inferred automatically
            game: new Game()
        }
    },
    getters: {
        getDeck: (state) => state.game.getDeck,
        getPlayers: (state) : Map<string, Player> => state.game.getPlayers,
        getAnnouncement: (state) => state.game.getAnnouncement
    },
    actions: {
        drawCard() : void {
            this.game.drawCards();
        },
        rematch() : void {
            this.game = new Game();
        }
    }

})